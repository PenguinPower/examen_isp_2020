import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Ex1 extends JFrame {
    JTextField a,b,p;
    JButton x;
    JLabel p1,p2,p3;
    Ex1()
    {
        setSize(350,400);
        setLayout(null);
        setTitle("Produsul a doua numere");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        a = new JTextField();
        a.setBounds(100,50,100,50);
        b = new JTextField();
        b.setBounds(100,120,100,50);
        p = new JTextField();
        p.setBounds(50,200,200,50);
        p.setEnabled(false);
        x = new JButton("X");
        x.setBounds(100,270,100,30);
        add(a);add(b);add(p);add(x);
        p1 = new JLabel("x");
        p2 = new JLabel("y");
        p3 = new JLabel("P");
        p1.setBounds(70,50,20,50);
        p2.setBounds(70,120,20,50);
        p3.setBounds(20,200,20,50);
        add(p1);add(p2);add(p3);
        x.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    int x1 = Integer.parseInt(a.getText()), x2 = Integer.parseInt(b.getText());
                    int prod = x1 * x2;
                    p.setText(String.valueOf(prod));
                } catch (NumberFormatException e) {
                    int x1 = 0, x2 = 0, prod = 0;
                    p.setText("Introduceti numere!");
                }
            }
        });
        setVisible(true);
    }

    public static void main(String[] args) {
        new Ex1();
    }
}
